<!doctype html>
<html lang="en">
  <head>
    <title>Writer</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <?php
        require './connection.php';
        $sql = "select * from writer_tb";
        $result = mysqli_query($connection,$sql);    
        $result = $result->fetch_all(MYSQLI_ASSOC); 
        // var_dump($result);
    ?> 
  </head>
  <body>
    <div class="container mt-5">
      <div class="d-flex justify-content-between">
        <div>
          <h4>Writer</h4>
        </div>
        <div>
          <a name="" id="" class="btn btn-primary" href="4.php" role="button">Book</a>          
          <a name="" id="" class="btn btn-primary" href="writer_list.php" role="button">Writer</a>
          <a name="" id="" class="btn btn-primary" href="category_list.php" role="button">Category</a>
        </div>
      </div>      
        <a name="" id="" class="btn btn-primary mt-2" href="add_writer.php" role="button">Add Writer</a>             
        <div class="row  mt-5"> 
            <?php
              foreach ($result as $key => $data):                            
            ?>            
                <div class="col-md-4 ">
                <div class="card shadow">                  
                  <div class="card-body">
                    <h4 class="card-title"><?= $data['name'] ?></h4>                    
                    <p>email : <?= $data['email'] ?></p>
                    <p>telp : <?= $data['telp'] ?></p>
                  </div>
                  <div class="d-flex justify-content-between mx-2 mb-2">
                    <a id="" class="btn btn-primary" href="<?= "./writer_detail.php?id=".$data['id'] ?>" role="button">View Detail</a>
                    <a id="" class="btn btn-success" href="<?= "./writer_edit.php?id=".$data['id'] ?>" role="button">Edit</a>
                    <a id="" class="btn btn-danger" href="<?= "./writer_delete.php?id=".$data['id'] ?>" role="button">Delete</a>
                  </div>
                </div>                    
                </div>
              <?php endforeach; ?>
        </div>      
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>