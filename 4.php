<!doctype html>
<html lang="en">
  <head>
    <title>Book</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <?php
        require './connection.php';
        $sql = "select *,book_tb.name as book_name,book_tb.id as id_book from book_tb inner join writer_tb on book_tb.writer_id=writer_tb.id";
        $result = mysqli_query($connection,$sql);    
        $result = $result->fetch_all(MYSQLI_ASSOC); 
        // var_dump($result);
    ?> 
  </head>
  <body>
    <div class="container mt-5">
      <div class="d-flex justify-content-between">
        <div>
          <h4>Books</h4>
        </div>
        <div>
          <a name="" id="" class="btn btn-primary" href="add_book.php" role="button">Add Book</a>
          <a name="" id="" class="btn btn-primary" href="category_list.php" role="button">Category</a>
          <a name="" id="" class="btn btn-primary" href="writer_list.php" role="button">Writer</a>
        </div>
      </div>      
        <div class="row  mt-5">              
            <?php
              foreach ($result as $key => $data):                            
            ?>            
                <div class="col-md-4 ">
                <div class="card shadow">
                  <img class="card-img-top" height="200" src="<?= "./". $data['img'] ?>" alt="">
                  <div class="card-body">
                    <h4 class="card-title"><?= $data['book_name'] ?></h4>
                    <div class="d-flex justify-content-between">
                      <div>
                        <?= $data['publication_year']?>
                      </div>
                      <div>
                        <?= $data['name']?>
                      </div>
                    </div>
                  </div>
                  <div class="d-flex justify-content-between mx-2 mb-2">
                    <a id="" class="btn btn-primary" href="<?= "./book_detail.php?id=".$data['id_book'] ?>" role="button">View Detail</a>
                    <a id="" class="btn btn-success" href="<?= "./book_edit.php?id=".$data['id_book'] ?>" role="button">Edit</a>
                    <a id="" class="btn btn-danger" href="<?= "./book_delete.php?id=".$data['id_book'] ?>" role="button">Delete</a>
                  </div>
                </div>                    
                </div>
              <?php endforeach; ?>
        </div>      
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>