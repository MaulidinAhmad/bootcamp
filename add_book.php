<!doctype html>
<html lang="en">
  <head>
    <title>Book</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <?php
        require './connection.php';        
        $sql1 = "select * from category_tb";                
        $sql2 = "select * from writer_tb";                
        $categories = $connection->query($sql1);
        $writers = $connection->query($sql2);
        // var_dump($result->fetch_assoc());
    
        // var_dump($result->fetch_assoc());
    ?> 
  </head>
  <body>
    <div class="container mt-5">
      <div class="d-flex justify-content-between">
        <div>
          <h4>Add Book</h4>
        </div>
        <div>
          <a name="" id="" class="btn btn-primary" href="4.php" role="button">Book List</a>
          <a name="" id="" class="btn btn-primary" href="add_category.php" role="button">Add Category</a>
          <a name="" id="" class="btn btn-primary" href="add_writer.php" role="button">Add Writer</a>
        </div>
      </div>      
        <div class="row justify-content-center mt-5">                
            <div class="col-md-8">
                <div class="card shadow mb-4">                    
                    <div class="card-body">
                        <h4 class="card-title">Add Book</h4>
                        <form action="add_book_process.php" enctype="multipart/form-data" method="post">
                            <div class="form-group">
                              <label for="">Name</label>
                              <input type="text"
                                class="form-control" name="name" id="name" aria-describedby="helpId" placeholder="">
                            </div>
                            <div class="form-group">
                              <label for="">Category</label>
                              <select class="form-control" name="category" id="">
                              <?php foreach($categories->fetch_all(MYSQLI_ASSOC) as $data): ?>
                                <option value="<?= $data['id'] ?>"><?= $data['name'] ?></option>                                
                              <?php endforeach; ?>
                              </select>
                            </div>
                            <div class="form-group">
                              <label for="">Writer</label>
                              <select class="form-control" name="writer" id="">
                              <?php foreach($writers->fetch_all(MYSQLI_ASSOC) as $data): ?>
                                <option value="<?= $data['id'] ?>"><?= $data['name'] ?></option>                                
                              <?php endforeach; ?>                         
                              </select>
                            </div>
                            <div class="form-group">
                              <label for="">Publication Year</label>
                              <input type="text"
                                class="form-control" name="publication_year" id="" aria-describedby="helpId" placeholder="">                              
                            </div>
                            <div class="form-group">
                              <label for="">Image</label>
                              <input type="file" class="form-control-file" name="image" id="" placeholder="" aria-describedby="fileHelpId">                              
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>      
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>